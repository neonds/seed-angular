![Sin nombre.png](https://bitbucket.org/repo/Abb5yL/images/1445867360-Sin%20nombre.png)

#SEED ANGULARJS - PROJECT TEMPLATE #
Version: 0.0.1-ALPHA
## RESUMEN ##
## JQuery y Ajax ##


 Ajax es una forma de hacer llamados asincronos a un servidor http/s y JQuery es una librería que está "on top layer"  sobre los 3 navegadores más importantes, que facilitan el manejo de Javascript.

## AngularJS ##

Angular es un Framework MVC Javascript que se ejecuta unicamente del lado del cliente y que permite desarrollar aplicaciones modernas de una sola página (Single Web Page ) .

JQuery no es un framework, y utilizar Ajax sin una organización de código puede convertirse en algo inmanejable y sin sentido. (casi como programar PHP o JSP sin una organización de capas). En otras palabras código espaguetti.

Angular permite organizar el código Javascript en Modelos, Vistas Controladores, Servicios, Rutas entre otros. La magia sucede cuando se consumen servicios Restfull (serian los datasources). Es posible fusionar diferentes API's de distintas aplicaciones o "Vendors" para crear Ambiciosas aplicaciones (building blocks).

Este "project template" es uno de muchos proyectos semilla que hay. No promete mucho pero pretende ser el más sencillo.

### Initial Committer ###
Guillermo B Díaz Solís (neonds)


## Guía Rápida del Desarrollador ##

### 1 Descargar e instalar ###

* nodejs v4.4.1 [página oficial](https://nodejs.org)

### 2 Clonar este proyecto ###

```
#!shell

$ git clone https://bitbucket.org/neonds/seed-angular
```


### 3 Entrar al directorio seed angular ###

### 4 Instalar dependencias de construcción ###

```
#!shell

$ npm install -g bower 
$ npm install -g grunt-cli 
$ npm install
```

### 5 Instalar dependencias del proyecto front end ###

```
#!shell
 $ bower install
```

### 6 Comandos del desarrollador ###


```
#!shell

$ grunt run
```
  Esto construirá el proyecto y levantará un servidor local mostrando la pagina de incicio de la aplicación.


### Estructura de directorios ###
![Captura.PNG](https://bitbucket.org/repo/Abb5yL/images/2586574954-Captura.PNG)


## ¿Qué viene después? ##

Esta es una versión muy tierna, sin embargo con el tiempo puede mejorar.
### Entre tanto falta: ###
* Internacionalización (mensajes en/es/fr, etc)
* Test unitarios con Karma
* Mejorar la forma en la que grunt-contrib-watch, detecta los cambios en los archivos para recompilar.
* Configuración para Integración Continua (Jenkins, Bamboo etc)
* Ofuscación y compresión de javascript (por ahora todo va al directorio build) Sería genial tener un directorio dist con Javascript minificado.
* Documentación Formal
* Configuración de: [Freddie Server](https://www.npmjs.com/package/freddie)
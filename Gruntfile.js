/**
 * Author: Guillermo B Díaz Solís
 * Since: Abril, 2016
 **/

module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-html-build');
    grunt.loadNpmTasks('grunt-include-source');
    grunt.loadNpmTasks('grunt-http-server');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-jshint');



    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        project: {
            src: 'app',
            vendor: 'bower_components',
            build: 'build',
            dist: 'dist'
        },

        copy: {
            build_font: {
                expand: true,
                cwd: '<%= project.src %>/assets/fonts',
                src: '*.ttf',
                dest: '<%= project.build %>/assets/fonts/'
            },

            build_img: {
                expand: true,
                cwd: '<%= project.src %>/assets/img',
                src: '*.{jpg, png, gif}',
                dest: '<%= project.build %>/assets/img/'
            },
            copy_vendor: {
                files: [
                    {expand: true, cwd: '<%= project.vendor %>/angular/', src: 'angular.min.js', dest: '<%= project.build %>/assets/vendor/js'},
                    {expand: true, cwd: '<%= project.vendor %>/angular-route/', src: 'angular-route.min.js', dest: '<%= project.build %>/assets/vendor/js'},
                    {expand: true, cwd: '<%= project.vendor %>/jquery/dist', src: 'jquery.min.js', dest: '<%= project.build %>/assets/vendor/js'},
                    {expand: true, cwd: '<%= project.vendor %>/foundation/css', src: 'foundation.min.css', dest: '<%= project.build %>/assets/vendor/css'},                   
                    {expand: true, cwd: '<%= project.vendor %>/foundation/js/', src: 'foundation.min.js', dest: '<%= project.build %>/assets/vendor/js'},
                    {expand: true, cwd: '<%= project.vendor %>/foundation/js/foundation', src: '*', dest: '<%= project.build %>/assets/vendor/js'},


                ]
            },

        },

        concat: {
            options: {
              separator: '\n\n',
            },

            build_js: {
                src: ['<%= project.src %>/app.js',
	                '<%= project.src %>/routes.js', 
	                '<%= project.src %>/controllers/**/*.js',
	                '<%= project.src %>/services/**/*.js'],
                dest: '<%= project.build %>/assets/js/<%= pkg.name %>-<%= pkg.version %>.js'
            },

            build_css: {
                src: [
                '<%= project.src %>/assets/css/**/*.css'
                ],

                dest: '<%= project.build %>/assets/css/<%= pkg.name %>-<%= pkg.version %>.css'
            },
            build_app: {
                src: [
                    '<%= project.vendor %>/jquery/dist/jquery.min.js',
                    '<%= project.vendor %>/foundation/js/foundation.min.js',      
                    '<%= project.vendor %>/angular/angular.min.js',
                    '<%= project.build %>/assets/js/<%= pkg.name %>-<%= pkg.version %>.js',
                    '<%= project.build %>/assets/js/<%= pkg.name %>-<%= pkg.version %>.js',

                ],
                dest: '<%= project.build %>/scripts/<%= pkg.name %>-<%= pkg.version %>.js'
            },
        },

        includeSource: {
          options: {
            basePath: 'build',
            baseUrl: '',
            templates: {
              html: {
                js: '<script src="{filePath}"></script>',
                css: '<link rel="stylesheet" type="text/css" href="{filePath}" />',
              },
            }
          },
          myTarget: {
            files: {
              'build/index.html': 'app/layout/default/default.html'
            }
          }
        },

          html2js: {
            options: {
            base: 'app',
            module: 'templates',
            singleModule: true,
            useStrict: true,
              htmlmin: {
                  collapseBooleanAttributes: true,
                  collapseWhitespace: true,
                  removeAttributeQuotes: true,
                  removeComments: true,
                  removeEmptyAttributes: true,
                  removeRedundantAttributes: true,
                  removeScriptTypeAttributes: true,
                  removeStyleLinkTypeAttributes: true
              },
            },
            main: {
              src: ['app/views/**/*.html'],
              dest: 'build/assets/js/views-<%= pkg.version %>.js'
            },
          },

        clean: {
            dist: '<%= project.dist %>',
            build: '<%= project.build %>'
        },


        'http-server': {
     
            dev: {
                root: '<%= project.build %>',
                port: 4000,
                host: "localhost",
                cache:  60,
                showDir : true,
                autoIndex: true,
                ext: "html",
                runInBackground: true,
                openBrowser: true,
            }
     
        },
     
        watch: {
            options: {
            },
            scripts: {
                    
                    files: 'app/**/*.*',
                    tasks: ['run'],
                    options: {
                        interrupt: true,
                },
            },

        },

      jshint: {
        all: ['Gruntfile.js', '<%= project.src %>/app.js','<%= project.src %>/controllers/**/*.js', '<%= project.src %>/services/**/*.js', 'test/**/*.js']
      },

    });

    grunt.registerTask('build', function() {
        grunt.task.run('clean');
        grunt.task.run('jshint');
        grunt.task.run('copy:build_font');
        grunt.task.run('copy:build_img');
        grunt.task.run('copy:copy_vendor');
        grunt.task.run('concat:build_js');
        grunt.task.run('html2js');
        grunt.task.run('concat:build_css');
        grunt.task.run('includeSource');

        
    });

    grunt.registerTask('run', function() {
        grunt.task.run('build');
        grunt.task.run('http-server:dev');
        grunt.task.run('watch:scripts');
    });


};